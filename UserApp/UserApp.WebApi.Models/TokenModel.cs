﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp.WebApi.Models
{
    public class TokenModel
    {
        [Required(ErrorMessage = "User name is required !!!")]
        [MaxLength(20)]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password is required !!!")]
        [MaxLength(20)]
        public string? Password { get; set; }
    }
}
