﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using UserApp.Services;

namespace UserApp.WebApi.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "User name is required !!!")]
        [MaxLength(20)]
        public string? Username { get; set; }

        [Required(ErrorMessage = "Password is required !!!")]
        [MaxLength(20)]
        public string? Password { get; set; }

        [Required(ErrorMessage = "Email is required !!!")]
        [MaxLength(20)]
        public string? Email { get; set; }

        public bool IsActive { get; set; }

        [JsonIgnore]
        public UserProfile? UserProfile { get; set; }

    }
}
