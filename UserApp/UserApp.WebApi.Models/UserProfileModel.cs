﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using UserApp.Services;

namespace UserApp.WebApi.Models
{
    public class UserProfileModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Firstname is required !!!")]
        public string? Firstname { get; set; }

        [Required(ErrorMessage = "Lastname is required !!!")]
        public string? Lastname { get; set; }

        [StringLength(11, MinimumLength = 11, ErrorMessage = "Personal numbers must contains 11 characters. ")]
        public string? PersonalNumber { get; set; }

        [JsonIgnore]
        public User? User { get; set; }
    }
}
