﻿using Microsoft.EntityFrameworkCore;

namespace UserApp.Services.Database
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; } = null!;

        public DbSet<UserProfile> UserProfiles { get; set; } = null!;
    }
}
