﻿using Microsoft.EntityFrameworkCore;
using UserApp.Services.Interfaces;

namespace UserApp.Services.Database.Repository
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly Context context;

        public UserProfileRepository(Context context)
        {
            this.context = context;
        }
        public async Task<bool> CreateUserProfile(UserProfile userProfile)
        {
            _ = await context.AddAsync(userProfile);
            return await Save();
        }

        public async Task<bool> DeleteUserProfile(UserProfile userProfile)
        {
            _ = context.Remove(userProfile);
            return await Save();
        }

        public async Task<bool> EditUserProfile(UserProfile userProfile)
        {
            _ = context.Update(userProfile);
            return await Save();
        }

        public async Task<UserProfile> GetUserProfile(int userId)
        {
            return await context.UserProfiles.Where(u => u.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task<ICollection<UserProfile>> GetUsersProfiles()
        {
            return await context.UserProfiles.OrderBy(u => u.UserId).ToListAsync();
        }

        public async Task<bool> Save()
        {
            var saved = await context.SaveChangesAsync();
            return saved > 0;
        }

        public async Task<bool> UserProfileExsists(int userId)
        {
            return await context.UserProfiles.AnyAsync(u => u.UserId == userId);
        }
    }
}
