﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserApp.Services.Interfaces;

namespace UserApp.Services.Database.Repository
{
    public class TokenRepository : ITokenRepository
    {
        private readonly Context context;

        public TokenRepository(Context context)
        {
            this.context = context;
        }
        public async Task<User> GetUser(User user)
        {
            return await context.Users.FirstOrDefaultAsync(u => u.Username == user.Username && u.Password == user.Password);
        }
    }
}
