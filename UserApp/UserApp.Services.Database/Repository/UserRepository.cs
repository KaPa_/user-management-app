﻿using Microsoft.EntityFrameworkCore;
using UserApp.Services.Interfaces;

namespace UserApp.Services.Database.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly Context context;

        public UserRepository(Context context)
        {
            this.context = context;
        }

        public async Task<bool> CreateUser(User user)
        {
            _ = await context.AddAsync(user);
            return await Save();
        }

        public async Task<bool> DeleteUser(User user)
        {
            _ = context.Remove(user);
            return await Save();
        }

        public async Task<bool> EditUser(User user)
        {
            _ = context.Update(user);
            return await Save();
        }

        public async Task<UserProfile> GetProfileByUser(int userId)
        {
            return await context.UserProfiles.Where(p => p.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task<User> GetUser(int userId)
        {
            return await context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();
        }

        public async Task<ICollection<User>> GetUsers()
        {
            return await context.Users.OrderBy(u => u.Id).ToListAsync();
        }

        public async Task<bool> Save()
        {
            var saved = await context.SaveChangesAsync();
            return saved > 0;
        }

        public async Task<bool> UserExsists(int userId)
        {
            return await context.Users.AnyAsync(u => u.Id == userId);
        }
    }
}
