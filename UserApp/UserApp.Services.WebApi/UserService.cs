﻿using System.Net.Http.Json;
using System.Net.Mime;
using System.Reflection.Metadata;
using System.Text.Json;
using System.Text;
using UserApp.Services.Interfaces;
using System.Net.Http.Headers;

namespace UserApp.Services.WebApi
{
    public class UserService : IUserService
    {
        private readonly HttpClient httpClient;

        public UserService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<bool> CreateUSer(User user)
        {

            using var client = httpClient;
            var apiUriCreate = ApiEndpoints.PostUserEndpint;

            var json = JsonSerializer.Serialize(user, new JsonSerializerOptions(JsonSerializerDefaults.Web));
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PostAsync(apiUriCreate, content);

            return response.IsSuccessStatusCode;

        }

        public async Task<bool> DeleteUSer(int id, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriDelete = ApiEndpoints.DeleteUserEndpoint(id);

            var response = await client.DeleteAsync(apiUriDelete);

            return response.IsSuccessStatusCode;

        }

        public async Task<bool> EditUSer(User user, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriEdit = ApiEndpoints.PutUserEndpoint(user.Id);

            var json = JsonSerializer.Serialize(user, new JsonSerializerOptions(JsonSerializerDefaults.Web));
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PutAsync(apiUriEdit, content);

            return response.IsSuccessStatusCode;

        }

        public async Task<User?> GetDeleteView(int id, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriDelete = ApiEndpoints.DeleteUserEndpoint(id);

            var response = await client.GetAsync(apiUriDelete);

            if (response.IsSuccessStatusCode)
            {
                var user = await response.Content.ReadFromJsonAsync<User>();
                return user;
            }

            return null;

        }

        public async Task<User?> GetEditView(int id, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUri = ApiEndpoints.PutUserEndpoint(id); ;

            var response = await client.GetAsync(apiUri);

            if (response.IsSuccessStatusCode)
            {
                var user = await response.Content.ReadFromJsonAsync<User>();

                return user;
            }

            return null;

        }

        public Task<User> GetUser(int UserId)
        {
            throw new NotImplementedException();
        }

        public async Task<ICollection<User>> GetUSers(string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUri = ApiEndpoints.GetUsersEndpint;

            var response = await client.GetAsync(apiUri);

            if (response.IsSuccessStatusCode)
            {
                var users = await response.Content.ReadFromJsonAsync<List<User>>();
                return users!;
            }

            return null!;
        }
    }
}
