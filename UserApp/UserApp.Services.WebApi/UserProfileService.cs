﻿using System.Net.Http.Json;
using System.Net.Mime;
using System.Text.Json;
using System.Text;
using UserApp.Services.Interfaces;
using System.Net.Http.Headers;

namespace UserApp.Services.WebApi
{
    public class UserProfileService : IUserProfileService
    {
        private readonly HttpClient httpClient;

        public UserProfileService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<bool> CreateUSerProfile(UserProfile userProfile, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriCreate = ApiEndpoints.UserProfileEndpoint(userProfile.UserId);

            var json = JsonSerializer.Serialize(userProfile, new JsonSerializerOptions(JsonSerializerDefaults.Web));
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PostAsync(apiUriCreate, content);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteUSerProfile(int userId, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriDelete = ApiEndpoints.UserProfileEndpoint(userId);

            var response = await client.DeleteAsync(apiUriDelete);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> EditUSerProfile(UserProfile userProfile, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriEdit = ApiEndpoints.UserProfileEndpoint(userProfile.UserId);

            var json = JsonSerializer.Serialize(userProfile, new JsonSerializerOptions(JsonSerializerDefaults.Web));
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PutAsync(apiUriEdit, content);

            return response.IsSuccessStatusCode;
        }

        public async Task<UserProfile?> GetDeleteView(int userId, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUriDelete = ApiEndpoints.UserProfileEndpoint(userId);

            var response = await client.GetAsync(apiUriDelete);

            if (response.IsSuccessStatusCode)
            {
                var UserProfile = await response.Content.ReadFromJsonAsync<UserProfile>();
                return UserProfile;
            }

            return null;
        }

        public async Task<UserProfile?> GetEditView(int userId, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUri = ApiEndpoints.UserProfileEndpoint(userId); ;

            var response = await client.GetAsync(apiUri);

            if (response.IsSuccessStatusCode)
            {
                var userProfile = await response.Content.ReadFromJsonAsync<UserProfile>();

                return userProfile;
            }

            return null;
        }

        public async Task<UserProfile> GetUserProfile(int userId, string accessToken)
        {
            using var client = httpClient;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var apiUri = ApiEndpoints.UserProfileEndpoint(userId); ;

            var response = await client.GetAsync(apiUri);

            if (response.IsSuccessStatusCode)
            {
                var userProfile = await response.Content.ReadFromJsonAsync<UserProfile>();

                return userProfile!;
            }

            return null!;
        }
    }
}
