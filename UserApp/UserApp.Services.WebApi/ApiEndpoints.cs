﻿namespace UserApp.Services.WebApi
{
    public class ApiEndpoints
    {
        public static string GetUsersEndpint { get; } = $"http://localhost:5015/api/User";

        public static string PostUserEndpint { get; } = $"http://localhost:5015/api/user/create";

        public static string Login { get; } = $"http://localhost:5015/api/token/login";

        public static string GetUserEndpoint ( int userId )
        {
            return $"http://localhost:5015/api/User{userId}";
        }

        public static string PutUserEndpoint(int userId)
        {
            return $"http://localhost:5015/api/User/{userId}";
        }

        public static string DeleteUserEndpoint(int userId)
        {
            return $"http://localhost:5015/api/User/{userId}";
        }

        public static string PostUserProfileEndpint { get; } = $"http://localhost:5015/api/UserProfile";

        public static string UserProfileEndpoint(int userId)
        {
            return $"http://localhost:5015/api/UserProfile/{userId}";
        }

    }
}
