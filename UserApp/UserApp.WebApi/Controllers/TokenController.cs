﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using UserApp.Services;
using UserApp.Services.Interfaces;
using UserApp.WebApi.Models;

namespace UserApp.WebApi.Controllers
{
    [ApiController]
    public class TokenController : Controller
    {
        private readonly ITokenRepository tokenRepository;
        private readonly IConfiguration configuration;

        public TokenController(ITokenRepository tokenRepository, IConfiguration configuration)
        {
            this.tokenRepository = tokenRepository;
            this.configuration = configuration;
        }

        [HttpPost]
        [Route("api/token/login")]
        public async Task<IActionResult> Post([FromBody] TokenModel tokenModel)
        {
            if (tokenModel != null && tokenModel.Username != null && tokenModel.Password != null)
            {
                var userMap = new User()
                {
                    Username = tokenModel.Username,
                    Password = tokenModel.Password
                };

                var user = await tokenRepository.GetUser(userMap);
                if (user != null)
                {
                    var claims = new[]
                   {
                    new Claim(JwtRegisteredClaimNames.Sub,configuration["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat,DateTime.UtcNow.ToString()),
                    new Claim("Id", user.Id.ToString()),
                    new Claim("Username", user.Username!),
                    new Claim("Password", user.Password!),
                };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var token = new JwtSecurityToken(
                        configuration["Jwt:Issuer"],
                        configuration["Jwt:Audience"],
                        claims,
                        expires: DateTime.Now.AddMinutes(20),
                        signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Invalid credentials");
                }
            }
            else
            {
                return BadRequest();
            }
            
        }
    }
}
