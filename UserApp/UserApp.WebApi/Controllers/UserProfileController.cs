﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserApp.Services;
using UserApp.Services.Interfaces;
using UserApp.WebApi.Models;

namespace UserApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserProfileController : Controller
    {
        private readonly IUserProfileRepository userProfileRepository;
        private readonly IMapper mapper;

        public UserProfileController(IUserProfileRepository userProfileRepository, IMapper mapper)
        {
            this.userProfileRepository = userProfileRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsersProfiles()
        {
            var usersProfiles = mapper.Map<List<UserProfileModel>>(await userProfileRepository.GetUsersProfiles());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(usersProfiles);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserProfile(int userId)
        {
            if (!await userProfileRepository.UserProfileExsists(userId))
                return NotFound();

            var userProfile = mapper.Map<UserProfileModel>(await userProfileRepository.GetUserProfile(userId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(userProfile);
        }

        [HttpPost("{userId}")]
        public async Task<IActionResult> CreateUserProfile([FromRoute]int userId, [FromBody] UserProfileModel UserProfileCreate)
        {
            if (UserProfileCreate == null)
                return BadRequest(ModelState);

            var usersProfiles = mapper.Map<List<UserProfileModel>>(await userProfileRepository.GetUsersProfiles());
            var userProfile = usersProfiles.FirstOrDefault(c => c.UserId == UserProfileCreate.UserId);

            if (userProfile != null)
            {
                ModelState.AddModelError(string.Empty, "user Profile already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userProfileMap = mapper.Map<UserProfile>(UserProfileCreate);
            userProfileMap.UserId = userId;

            if (!await userProfileRepository.CreateUserProfile(userProfileMap))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong while savin");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> EditUserProfile(int userId, [FromBody] UserProfileModel updatedUserProfile)
        {
            if (updatedUserProfile == null)
                return BadRequest(ModelState);

            if (userId != updatedUserProfile.UserId)
                return BadRequest(ModelState);

            if (!await userProfileRepository.UserProfileExsists(userId))
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest();

            var userProfileMap = mapper.Map<UserProfile>(updatedUserProfile);

            if (!await userProfileRepository.EditUserProfile(userProfileMap))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong updating user profile");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUserProfile(int userId)
        {
            if (!await userProfileRepository.UserProfileExsists(userId))
            {
                return NotFound();
            }

            var userProfileToDelete = await userProfileRepository.GetUserProfile(userId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await userProfileRepository.DeleteUserProfile(userProfileToDelete))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong deleting user Profile");
            }

            return NoContent();
        }
    }
}
