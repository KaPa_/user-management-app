﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using UserApp.Services;
using UserApp.Services.Interfaces;
using UserApp.WebApi.Models;

namespace UserApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly IUserProfileRepository userProfileRepository;
        private readonly IMapper mapper;
        private readonly IConfiguration configuration;

        public UserController(IUserRepository userRepository, IUserProfileRepository userProfileRepository, IMapper mapper, IConfiguration configuration)
        {
            this.userRepository = userRepository;
            this.userProfileRepository = userProfileRepository;
            this.mapper = mapper;
            this.configuration = configuration;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUsers()
        {
            var users = mapper.Map<List<UserModel>>(await userRepository.GetUsers());

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(users);
        }

        [HttpGet("{userId}")]
        [Authorize]
        public async Task<IActionResult> GetUser(int userId)
        {
            if (!await userRepository.UserExsists(userId))
                return NotFound();

            var user = mapper.Map<UserModel>(await userRepository.GetUser(userId));

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(user);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateUser([FromBody] UserModel UserCreate)
        {
            if (UserCreate == null)
                return BadRequest(ModelState);

            var users = mapper.Map<List<UserModel>>(await userRepository.GetUsers());
            var user = users.FirstOrDefault(c => c.Username!.Trim().ToUpper() == UserCreate.Username!.TrimEnd().ToUpper());

            if (user != null)
            {
                ModelState.AddModelError(string.Empty, "user name already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userMap = mapper.Map<User>(UserCreate);

            if (!await userRepository.CreateUser(userMap))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong while savin");
                return StatusCode(500, ModelState);
            }

            return Ok("Successfully created");
        }

        [Authorize]
        [HttpPut("{userId}")]
        public async Task<IActionResult> EditUser(int userId, [FromBody] UserModel updatedUser)
        {
            if (updatedUser == null)
                return BadRequest(ModelState);

            if (userId != updatedUser.Id)
                return BadRequest(ModelState);

            if (!await userRepository.UserExsists(userId))
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest();

            var userMap = mapper.Map<User>(updatedUser);

            if (!await userRepository.EditUser(userMap))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong updating user");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [Authorize]
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            if (!await userRepository.UserExsists(userId))
            {
                return NotFound();
            }

            var userToDelete = await userRepository.GetUser(userId);

            if (await userProfileRepository.UserProfileExsists(userId))
            {
                var userProfileToDelete = await userProfileRepository.GetUserProfile(userId);
                if (!await userProfileRepository.DeleteUserProfile(userProfileToDelete))
                {
                    ModelState.AddModelError(string.Empty, "Something went wrong deleting user profile");
                }
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (!await userRepository.DeleteUser(userToDelete))
            {
                ModelState.AddModelError(string.Empty, "Something went wrong deleting user");
            }

            return NoContent();
        }
    }
}
