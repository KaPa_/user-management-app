﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Text.Json;
using System.Text;
using UserApp.Services.WebApi;
using UserApp.Services;
using UserApp.WebApi.Models;

namespace UserApp.WebApp.Controllers
{
    public class TokenController : Controller
    {
        private readonly HttpClient httpClient;

        public TokenController(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(TokenModel TokenModel)
        {
            var user = new User()
            {
                Username = TokenModel.Username,
                Password = TokenModel.Password,
            };

            using var client = httpClient;
            var apiUriCreate = ApiEndpoints.Login;

            var json = JsonSerializer.Serialize(user, new JsonSerializerOptions(JsonSerializerDefaults.Web));
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PostAsync(apiUriCreate, content);

            var token = await response.Content.ReadAsStringAsync();
            HttpContext.Session.SetString("JWToken", token);

            return RedirectToAction("Index", "User");

        }

        public IActionResult Logoff()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Login", "Token");
        }
    }
}
