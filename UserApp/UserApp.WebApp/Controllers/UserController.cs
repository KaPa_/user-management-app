﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UserApp.Services;
using UserApp.Services.Interfaces;
using UserApp.WebApi.Models;

namespace UserApp.WebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var Users = mapper.Map<List<UserModel>>(await userService.GetUSers(accessToken));

            if (Users != null)
            {
                return this.View(Users);
            }

            return this.View();
        }

        public IActionResult Create()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserModel userModel)
        {
            var result = await userService.CreateUSer(this.mapper.Map<User>(userModel));

            if (result)
            {
                return this.RedirectToAction("Login", "Token");
            }

            return this.View("Create");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = mapper.Map<UserModel>(await userService.GetEditView(id, accessToken));

            if (result != null)
            {
                return this.View(result);
            }

            return this.View();
        }

        public async Task<IActionResult> Edit(UserModel updateUserModel)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = await userService.EditUSer(mapper.Map<User>(updateUserModel), accessToken);

            if (result)
            {
                return this.RedirectToAction("Index", "User");
            }

            return this.View("Edit");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = mapper.Map<UserModel>(await userService.GetDeleteView(id, accessToken));

            if (result != null)
            {
                return this.View(result);
            }

            return this.View();
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = await this.userService.DeleteUSer(id, accessToken);

            if (result)
            {
                return this.RedirectToAction("Index");
            }

            return this.RedirectToAction("Index");
        }


    }
}
