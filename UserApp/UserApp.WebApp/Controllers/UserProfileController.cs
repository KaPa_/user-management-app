﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UserApp.Services;
using UserApp.Services.Interfaces;
using UserApp.Services.WebApi;
using UserApp.WebApi.Models;

namespace UserApp.WebApp.Controllers
{
    public class UserProfileController : Controller
    {
        private readonly IUserProfileService userProfileService;
        private readonly IMapper mapper;

        public UserProfileController(IUserProfileService userProfileService, IMapper mapper)
        {
            this.userProfileService = userProfileService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var userProfile = mapper.Map<UserProfileModel>(await userProfileService.GetUserProfile(id, accessToken));

            if (userProfile != null)
            {
                return this.View(userProfile);
            }
            else
            {
                var helperuserProfile = new UserProfileModel
                {
                    UserId = id
                };

                return this.View(helperuserProfile);
            }
        }

        public IActionResult Create()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(int id, UserProfileModel userProfileModel)
        {
            userProfileModel.UserId = id;
            var accessToken = HttpContext.Session.GetString("JWToken");

            var result = await userProfileService.CreateUSerProfile(this.mapper.Map<UserProfile>(userProfileModel), accessToken);

            if (result)
            {
                return this.RedirectToAction("Index", "UserProfile", new { id });
            }

            return this.View("Create");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = mapper.Map<UserProfileModel>(await userProfileService.GetEditView(id, accessToken));

            if (result != null)
            {
                return this.View(result);
            }

            return this.View();
        }

        public async Task<IActionResult> Edit(int id, UserProfileModel updateUserProfileModel)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = await userProfileService.EditUSerProfile(mapper.Map<UserProfile>(updateUserProfileModel), accessToken);

            if (result)
            {
                return this.RedirectToAction("Index", "UserProfile", new { id });
            }

            return this.View("Edit");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = mapper.Map<UserProfileModel>(await userProfileService.GetDeleteView(id, accessToken));

            if (result != null)
            {
                return this.View(result);
            }

            return this.View();
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteUserProfile(int userId)
        {
            var accessToken = HttpContext.Session.GetString("JWToken");
            var result = await this.userProfileService.DeleteUSerProfile(userId, accessToken);

            if (result)
            {
                return this.RedirectToAction("Index", "UserProfile", new { userId });
            }

            return this.RedirectToAction("Index", "UserProfile", new { userId });
        }
    }
}
