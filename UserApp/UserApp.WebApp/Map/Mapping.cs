﻿using AutoMapper;
using UserApp.Services;
using UserApp.WebApi.Models;

namespace UserApp.WebApp.Map
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            _ = CreateMap<User, UserModel>();
            _ = CreateMap<UserModel, User>();
            _ = CreateMap<UserProfile, UserProfileModel>();
            _ = CreateMap<UserProfileModel, UserProfile>();
        }
    }
}
