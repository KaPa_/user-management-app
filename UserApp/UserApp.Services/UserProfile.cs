﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace UserApp.Services
{
    public class UserProfile
    {
        [Key]
        [ForeignKey("User")]
        public int UserId { get; set; }

        public string? Firstname { get; set; }

        public string? Lastname { get; set; }

        [StringLength(11, MinimumLength = 11, ErrorMessage = "Personal numbers must contains 11 characters. ")]
        public string? PersonalNumber { get; set; }

        [JsonIgnore]
        public User? User { get; set; }
    }
}
