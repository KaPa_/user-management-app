﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp.Services.Interfaces
{
    public interface ITokenRepository
    {
        Task<User> GetUser(User user);
    }
}
