﻿namespace UserApp.Services.Interfaces
{
    public interface IUserProfileService
    {
        Task<UserProfile> GetUserProfile(int userId, string accessToken);

        Task<bool> CreateUSerProfile(UserProfile userProfile, string accesseToken);

        Task<UserProfile?> GetEditView(int userId, string accesseToken);

        Task<bool> EditUSerProfile(UserProfile userProfile, string accesseToken);

        Task<UserProfile?> GetDeleteView(int userId, string accesseToken);

        Task<bool> DeleteUSerProfile(int userId, string accesseToken);
    }
}
