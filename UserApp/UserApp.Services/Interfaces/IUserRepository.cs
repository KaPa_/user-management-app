﻿namespace UserApp.Services.Interfaces
{
    public interface IUserRepository
    {
        Task<ICollection<User>> GetUsers();

        Task<User> GetUser(int userId);

        Task<UserProfile> GetProfileByUser(int userId);

        Task<bool> UserExsists(int userId);

        Task<bool> CreateUser(User user);

        Task<bool> EditUser(User user);

        Task<bool> DeleteUser(User user);

        Task<bool> Save();
    }
}
