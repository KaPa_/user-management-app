﻿namespace UserApp.Services.Interfaces
{
    public interface IUserProfileRepository
    {
        Task<ICollection<UserProfile>> GetUsersProfiles();

        Task<UserProfile> GetUserProfile(int userId);

        Task<bool> UserProfileExsists(int userId);

        Task<bool> CreateUserProfile(UserProfile userProfile);

        Task<bool> EditUserProfile(UserProfile userProfile);

        Task<bool> DeleteUserProfile(UserProfile userProfile);

        Task<bool> Save();
    }
}
