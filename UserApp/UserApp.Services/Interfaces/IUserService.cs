﻿namespace UserApp.Services.Interfaces
{
    public interface IUserService
    {
        Task<ICollection<User>> GetUSers(string accessToken);

        Task<User> GetUser(int UserId);

        Task<bool> CreateUSer(User user);

        Task<User?> GetEditView(int id, string accessToken);

        Task<bool> EditUSer(User user, string accessToken);

        Task<User?> GetDeleteView(int id, string accessToken);

        Task<bool> DeleteUSer(int id, string accessToken);
    }
}
