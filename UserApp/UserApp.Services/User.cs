﻿using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Text.Json.Serialization;

namespace UserApp.Services
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string? Username { get; set; }

        public string? Password { get; set; }

        public string? Email { get; set; }

        public bool IsActive { get; set; } = false;

        [JsonIgnore]
        public UserProfile? UserProfile { get; set; }

    }
}
